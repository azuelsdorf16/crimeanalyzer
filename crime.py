import pandas as pd
import bs4
import requests
import re
import urllib
import os
import random
import time

def main():
    random.seed(time.time())

    block_list = ["Phone_Directory.pdf", "ADA_Notice.pdf"]
    base_url = "https://www.howardcountymd.gov/"
    url = "https://www.howardcountymd.gov/Departments/Police/Police-Newsroom/Crime-Bulletin"

    request_delay_bounds = (10, 20) #Wait 10 to 20 seconds between requests
    timeout_delay_bounds = (20, 30) #Wait 20 to 30 seconds between repeated requests.

    data_dir = "data"

    if not os.path.isdir(data_dir):
        os.makedirs(data_dir)

    response = requests.get(url, allow_redirects=True)

    html = bs4.BeautifulSoup(response.text, "html.parser")

    for tag in html.find_all("a"):
        if "href" in tag.attrs:
            matches = re.findall(r"(PoliceDailyCrimeBulletin[0-9]+\.pdf)", tag["href"])
            matches.extend(re.findall(r"(.*\/LinkClick\.aspx\?fileticket.*)", tag["href"]))

            if matches != []:
                url = None

                if tag["href"].startswith("http"):
                    url = str(tag["href"])
                else:
                    url = base_url + str(tag["href"])

                file_name = re.sub(r"[\s+,\.]+", "_", str(tag.text)) + ".pdf"

                file_path = os.path.join(data_dir, file_name)

                if (not os.path.exists(file_path)) and (file_name not in block_list):
                    print("Downloading \"{0}\" to \"{1}\"".format(url, file_path))
                    while True:
                        try:
                            delay = random.randint(*request_delay_bounds)
                            print("Waiting {0} seconds to avoid overwhelming website.".format(delay))
                            time.sleep(delay)
                            response = requests.get(url, allow_redirects=True)
                            break
                        except KeyboardInterrupt as ke:
                            raise ke
                        except:
                            delay = random.randint(*timeout_delay_bounds)
                            print("Could not download \"{0}\". Retrying in {1} seconds.".format(url, delay))
                            time.sleep(delay)

                    output_file = open(file_path, "wb")

                    output_file.write(response.content)

                    output_file.close()
                else:
                    if file_name in block_list:
                        print("Skipping \"{0}\" because it is in the block list.".format(url))
                    else:
                        print("Skipping \"{0}\" because it already exists at {1}.".format(url, file_path))

if __name__ == "__main__":
    main()
