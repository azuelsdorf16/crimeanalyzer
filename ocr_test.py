import ocr_util
import os

pdf_file = os.path.join("data", "Mon_Nov_7_2016.pdf")

text = ocr_util.perform_ocr_pdf(pdf_file)

print(text)
