import argparse
import os
import sys
import csv
import re
from opencage.geocoder import OpenCageGeocode, UnknownError
import ocr_util
import multiprocessing as mp

API_KEY = '1d4f71dd89934e898d1ebd1542837cc5'

header = ["block number", "street", "town", "zipcode", "day", "month", "year",
"time", "latitude", "longitude", "crime_type", "pdf"]

class GeocodeUtil(object):
    #TODO: [AWZ] Add caching functionality.

    def __init__(self, key, debug=False):
        self.key = str(key)
        self.debug = debug
        self.geocoder = OpenCageGeocode(self.key)

    def get_lat_lon_batch(self, addresses):
        lat_lons = dict()

        for address in addresses:
            lat_lons[address] = self.get_lat_lon(address)

        return lat_lons

    def get_lat_lon(self, address):
        #Return a null value if debugging.
        if self.debug:
            return (None, None)

        try:
            response = self.geocoder.geocode(address)
        except UnknownError:
            return (None, None)

        if response and len(response) > 0:
            if "geometry" not in response[0]:
                return (None, None)

            geometry = response[0]["geometry"]

            if "lat" in geometry:
                latitude = geometry["lat"]
            else:
                latitude = None

            if "lng" in geometry:
                longitude = geometry["lng"]
            else:
                longitude = None

            return (latitude, longitude)
        else:
            return (None, None)

def get_incident_info(text, geocode_util, year, pdf_file_path):
    CRIME_TYPES = [r"street\s+robbery", r"robbery", r"commercials\+burglary", r"residential\s+burglary", r"vehicle\s+theft", r"vehicle\s+theft\s*,\s+attempts", r"theft\s+from\s+vehicle", r"theft\s+from\s+vehicle\/vehicle\s+break\-in", r"indecent\s+exposure"]

    lines = [_ for _ in re.split(r"\n", text)]

    #Remove first four lines of PDF since the first four lines of the HoCo
    #crime report PDF doesn't provide any useful information.

    if len(lines) > 4:
        lines = lines[4:]

    previous_line = str(lines[0])

    crime_type = None

    incident_info = list()

    address_time_regex = r"([A-Za-z]+),\s+([0-9]+)\:\s+([0-9]+)\s+block\s+of\s+([A-Za-z\s]+),\s+([a-zA-Z]+)[\.\s]*([0-9]+)\s+(.*)"

    for line in lines[1:]:
        matches = re.findall(address_time_regex, line, flags=re.I)

        if len(matches) == 0:
            previous_line = str(line)
            continue
        elif len(matches) == 1:
            if previous_line.strip() != "":
                crime_type = str(previous_line)
        else:
            print("Found multiple matches ({1}) in line \"{0}\"".format(matches, line))

        town, zipcode, block_number, street, month, day, time_i = matches[0]

        town = re.sub(r"\s+", " ", town).strip()
        zipcode = re.sub(r"\s+", " ", zipcode).strip()
        block_number = re.sub(r"\s+", " ", block_number).strip()
        street = re.sub(r"\s+", " ", street).strip()
        month = re.sub(r"\s+", " ", month).strip()
        day = re.sub(r"\s+", " ", day).strip()
        time_i = re.sub(r"\s+", " ", time_i).strip()

        address = "{} {} {} {}".format(block_number, street, town, zipcode)

        lat, lon = geocode_util.get_lat_lon(address)

        info = dict()

        info["block number"] = block_number
        info["street"] = street
        info["town"] = town
        info["zipcode"] = zipcode
        info["day"] = day
        info["month"] = month
        info["year"] = year
        info["time"] = time_i
        info["latitude"] = lat
        info["longitude"] = lon
        info["pdf"] = str(pdf_file_path)
        info["crime_type"] = crime_type

        incident_info.append(info)

    return incident_info

def get_crime_info_from_pdf(args):
    """
    Get info on crimes from PDF file.
    """
    #Extract arguments into a usable format.
    geocode_util, file_i_path, debug = args

    if debug:
        print("Processing {0}".format(file_i_path))

    #Use Tesseract to extract raw text from PDF.
    pdf_text = ocr_util.perform_ocr_pdf(file_i_path)

    #In debug mode, write out intermediate results of tesseract's processing to a file.
    if debug:
        #Write out raw text that Tesseract extracted for debugging/logging purposes.
        raw_file_path = os.path.join('raw_text', os.path.basename(file_i_path))

        with open(raw_file_path, 'w', encoding="utf-8") as raw_file:
            raw_file.write(pdf_text)

        raw_file.close()

    incident_info = get_incident_info(pdf_text, geocode_util,
re.findall(r"_(2[0-9]{3})\.pdf", file_i_path)[0], file_i_path)

    return incident_info

def main():
    parser= argparse.ArgumentParser(description="""Parses Howard County crime
bulletins for address blocks, cities of crimes, and other useful crime-related
information. Puts output into a comma-separated value file that can be opened
in Excel, MATLAB, Python, or R for further analysis.""")

    parser.add_argument("-i", dest="input_path", required=False, help="Howard County crime bulletin PDF or directory of Howard County crime bulletin PDFs")
    parser.add_argument("-o", dest="output_file_dest", required=True, help="CSV file of information parsed from Howard County crime bulletins")
    parser.add_argument("--debug", dest="debug", required=False, default=False, action="store_true", help="Enable debugging (disable geolocation, disable multiprocessing, save raw text from Tesseract to a \"raw_text\" directory under the current directory)")

    args = parser.parse_args()

    if not os.path.exists(args.input_path):
        sys.stderr.write("ERROR: data directory \"{0}\" does not exist.\n".format(
args.input_path))
        sys.exit(1)

    try:
        output_file = open(args.output_file_dest, "w", encoding="utf8")
        output_file_writer = csv.writer(output_file, delimiter="\t", quotechar="|", quoting=csv.QUOTE_MINIMAL)
    except IOError as ioe:
        sys.stderr.write("Could not open output file \"{0}\": {1}\n".format(args.output_file_dest, str(ioe)))
        sys.exit(1)
    except OSError as ose:
        sys.stderr.write("Could not open output file \"{0}\": {1}\n".format(args.output_file_dest, str(ose)))
        sys.exit(1)
        
    output_file_writer.writerow(header)

    #TODO: Remove the "debug=True" when in production.
    geocode_util = GeocodeUtil(key=API_KEY, debug=False)#args.debug)

    print(args.input_path)

    if os.path.isdir(args.input_path):
        data_dir_files = [os.path.join(args.input_path, file_i) for file_i in os.listdir(args.input_path)]
    else:
        data_dir_files = [args.input_path]

    #Get a list of arguments to pass to our PDF processing function.
    processing_args = list()

    #Go through each file and subdir in directory.
    for file_i_path in data_dir_files:
        #Skip non-PDF files.
        if not file_i_path.endswith(".pdf"):
            #TODO: Replace this with the "magic" module's file type check for
            #PDF. File extension checking like this isn't a robust solution.
            print("Skipping {0} : not a PDF.".format(file_i_path))
            continue

        #Make sure the file is actually a file.
        if not os.path.isfile(file_i_path):
            print("Skipping {0} : not a file.".format(file_i_path))
        else:
            processing_args.append((geocode_util, file_i_path, args.debug))

    if args.debug:
        if not os.path.isdir('raw_text'):
            os.makedirs('raw_text')

        for incidents in map(get_crime_info_from_pdf, processing_args):
            for incident in incidents:
                incident_record = [str(incident[header_row]) for header_row in header]
                output_file_writer.writerow(incident_record)
    else:
        pool = mp.Pool()

        for incidents in pool.imap_unordered(get_crime_info_from_pdf, processing_args):
            for incident in incidents:
                incident_record = [str(incident[header_row]) for header_row in header]
                output_file_writer.writerow(incident_record)

        pool.close()

    output_file.close()

if __name__ == "__main__":
    main()
