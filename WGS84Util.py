from geographiclib.geodesic import Geodesic

geod = Geodesic.WGS84

def get_inverse_geodesic(lat_1, lon_1, lat_2, lon_2):
    """
    Given two pairs of a lat/lons, returns the s12 (shortest) distance
    between the points in kilometers.
    """

    return geod.Inverse(lat_1, lon_1, lat_2, lon_2)['s12'] / 1000

def get_shortest_distance(lat_1, lon_1, lat_2, lon_2):
    """
    A more intuitive name for the inverse geodesic function.
    """
    return get_inverse_geodesic(lat_1, lon_1, lat_2, lon_2)
