import pytesseract
from PIL import Image as PILImage
import subprocess
import shlex
import glob
import os
import platform

def perform_ocr_pdf(pdf_path):
    if "TESSDATA_PREFIX" not in os.environ and platform.system() == "Windows":
        os.environ["TESSDATA_PREFIX"] = "C:\\Program Files (x86)\\Tesseract-OCR\\tessdata\\"

        if not os.path.exists(os.environ["TESSDATA_PREFIX"]):
            return "Invalid TESSDATA_PREFIX environment variable. {0} does not exist on {1}".format(os.environ["TESSDATA_PREFIX"], platform.node())

    ghostscript_path = None

    if platform.system() == "Windows":
        ghostscript_path = "\"C:\\Program Files\\gs\\gs9.26\\bin\\gswin64c.exe\""

        if not os.path.exists(pytesseract.pytesseract.tesseract_cmd):
            pytesseract.pytesseract.tesseract_cmd = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe"
    else:
        ghostscript_path = "/usr/bin/ghostscript"

        if not os.path.exists(pytesseract.pytesseract.tesseract_cmd):
            pytesseract.pytesseract.tesseract_cmd = "/usr/bin/tesseract"

    if not os.path.exists(pytesseract.pytesseract.tesseract_cmd):
        return "Cannot find tesseract ({0} does not exist on {1})".format(pytesseract.pytesseract.tesseract_cmd, platform.node())

    if not os.path.exists(ghostscript_path):
        return "Cannot find ghostscript ({0} does not exist on {1})".format(ghostscript_path, platform.node())

    text = None

    subprocess.call(args=shlex.split("{2} -dNOPAUSE -sDEVICE=jpeg -r300 -dBATCH -q -sOutputFile={1} \"{0}\"".format(pdf_path, "p%03d.jpg", ghostscript_path)))

    text = ""

    for file_i in glob.glob("*.jpg"):
        with PILImage.open(file_i) as jpeg_image:
            text += pytesseract.image_to_string(jpeg_image, lang="eng")
        os.remove(file_i)

    return text
