from WGS84Util import get_shortest_distance

def main():
    print(get_shortest_distance(39.2058, -76.7531, 39.213772, -76.848589))

if __name__ == "__main__":
    main()
