from opencage.geocoder import OpenCageGeocode
import json

key = '1d4f71dd89934e898d1ebd1542837cc5'

geocoder = OpenCageGeocode(key)

query = '9000 Frederick Rd Ellicott City, 21042'

response = geocoder.geocode(query)

if response and len(response) > 0:
    for i, result in enumerate(response):
        print("Result {0}".format(i));
        print("-------------------------------")
        print(json.dumps(result, indent=4, separators=(",", ":")))
        print("-------------------------------")
